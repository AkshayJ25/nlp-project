package sentimentClassifier;

import de.bwaldvogel.liblinear.*;
import org.apache.commons.cli.BasicParser;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class ClassifierHelperHindi {
    List<LinkedHashMap<Integer, Double>> featureList = new ArrayList<LinkedHashMap<Integer, Double>>();

    ClassifierHelperHindi(String dataset) throws IOException {
        BufferedReader readerTrain = new BufferedReader(new FileReader(new File(dataset)));

        int count = 0;
        while (readerTrain.readLine() != null) {
            count++;
        }

        for (int i = 0; i < count; i++) {
            featureList.add(i, new LinkedHashMap<Integer, Double>());
        }

    }

    public void setHashMap(int start, List<LinkedHashMap<Integer, Double>> hMap) {
        for (int i = 0; i < hMap.size(); i++) {
            for (Map.Entry<Integer, Double> entry : hMap.get(i).entrySet()) {
                featureList.get(i).put(start + entry.getKey(), entry.getValue());
            }
        }
    }

    public List<LinkedHashMap<Integer, Double>> getList() {
        return featureList;
    }
}

public class SentimentClassifierHindi {

    static String rootDirectory;
    static List<LinkedHashMap<Integer, Double>> trainingFeature;
    static List<LinkedHashMap<Integer, Double>> testFeature;


    SentimentClassifierHindi(int option, String trainFile, String testFile) throws IOException {

        rootDirectory = System.getProperty("user.dir");
        mainClassifierFunction(option, trainFile, testFile);

        
    }

    private int generateFeature(int option, String trainFile, String testFile) throws IOException {
        ClassifierHelperHindi trainingObject = new ClassifierHelperHindi(rootDirectory+"\\dataset\\"+trainFile);


        ClassifierHelperHindi testObject = null;
        if(option == 2|| option == 3)
        {
            testObject = new ClassifierHelperHindi(rootDirectory+"\\dataset\\"+testFile);
        }
        int start = 0;
        POSHindi posObject = new POSHindi(rootDirectory);
        Ngram ngramObject = new Ngram(rootDirectory);
        trainingObject.setHashMap(start, ngramObject.getTrainingList());
        if(option == 2|| option == 3) {
            testObject.setHashMap(start, ngramObject.getTestList());
        }



        start += ngramObject.getFeatureCount();
        DTCOOCLexiconHindi dtCOOCObject = new DTCOOCLexiconHindi(rootDirectory);
        trainingObject.setHashMap(start, dtCOOCObject.getTrainingList());
        if(option == 2|| option == 3) {
            testObject.setHashMap(start, dtCOOCObject.getTestList());
        }



        trainingFeature = trainingObject.getList();
        if(option == 2|| option == 3) {
            testFeature = testObject.getList();
        }


        int finalSize = start + dtCOOCObject.getFeatureCount();

        return finalSize;


    }

        private void mainClassifierFunction(int option, String trainFile, String testFile)throws IOException {
            int finalSize = this.generateFeature(option, trainFile, testFile);

            System.out.println("Hello sentiment!");

            Problem problem = new Problem();

            double a[] = new double[this.trainingFeature.size()];
            File file = new File(rootDirectory + "\\dataset\\trainingLabels.txt");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String read;
            int count = 0;
            while ((read = reader.readLine()) != null) {
                a[count++] = Double.parseDouble(read.toString());
            }

            Feature[][] trainFeatureVector = new Feature[trainingFeature.size()][finalSize];

            for (int i = 0; i < trainingFeature.size(); i++) {
                System.out.println(i + " trained.");
                for (int j = 0; j < finalSize; j++) {
                    if (trainingFeature.get(i).containsKey(j + 1)) {
                        trainFeatureVector[i][j] = new FeatureNode(j + 1, trainingFeature.get(i).get(j + 1));
                    } else {
                        trainFeatureVector[i][j] = new FeatureNode(j + 1, 0.0);
                    }
                }
            }

            problem.l = trainingFeature.size(); // number of training examples
            problem.n = finalSize; // number of features
            problem.x = trainFeatureVector; // feature nodes
            problem.y = a; // target values ----

            BasicParser bp = new BasicParser();

            SolverType solver = SolverType.L2R_LR_DUAL; // -s 7
            double C = 1.0;    // cost of constraints violation
            double eps = 0.001; // stopping criteria

            Parameter parameter = new Parameter(solver, C, eps);
            Model model = Linear.train(problem, parameter);
            File modelFile = new File("model");
            model.save(modelFile);

            PrintWriter write = new PrintWriter(new BufferedWriter(new FileWriter(rootDirectory + "\\dataset\\predictedLabels.txt")));

            if (option == 1) {
                double[] val = new double[trainingFeature.size()];
                Linear.crossValidation(problem, parameter, 5, val);
                for (int i = 0; i < trainingFeature.size(); i++) {
                    write.println(val[i]);
                }
                write.close();
                return;
            }

            if (option == 2 || option == 3) {
                model = Model.load(modelFile);
                for (int i = 0; i < testFeature.size(); i++) {
                    Feature[] instance = new Feature[testFeature.get(i).size()];
                    int j = 0;
                    for (Map.Entry<Integer, Double> entry : testFeature.get(i).entrySet()) {
                        instance[j++] = new FeatureNode(entry.getKey(), entry.getValue());
                    }

                    double prediction = Linear.predict(model, instance);
                    write.println(prediction);
                }

                write.close();
                return;
            }
        }
}