package sentimentClassifier;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class POSBengali {
    String rootDirectory;

    POSBengali(String rootDirectory) throws IOException {
       this.rootDirectory = rootDirectory;

        generateDataset(rootDirectory + "\\dataset\\tokenized_Train.txt");
    }

    private void generateDataset(String input, String output)throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(input), "UTF-8"));
        Writer countWriter = new OutputStreamWriter(new FileOutputStream(output), "UTF-8");
        BufferedWriter fbw = new BufferedWriter(countWriter);

        String line;

        while((line = reader.readLine()) != null)
        {
            fbw.write(line+"\n");
        }

        fbw.close();

    }

}