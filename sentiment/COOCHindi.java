package sentimentClassifier;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class COOCHindi {
    List<LinkedHashMap<Integer, Double>> trainingFeature;
    List<LinkedHashMap<Integer, Double>> testFeature;
    String rootDirectory;
    int featureCount;

    COOCHindi(String rootDirectory) throws IOException {
        this.trainingFeature = new ArrayList<LinkedHashMap<Integer, Double>>();
        this.testFeature = new ArrayList<LinkedHashMap<Integer, Double>>();
        this.rootDirectory = rootDirectory;

        trainingFeature = generateFeature(rootDirectory + "\\dataset\\tokenized_Train.txt");
        
    }

    private List<LinkedHashMap<Integer, Double>> generateFeature(String fileName) throws IOException {
        List<LinkedHashMap<Integer, Double>> featureVector = new ArrayList<LinkedHashMap<Integer, Double>>();
        LinkedHashMap<String, Double> lexicon = new LinkedHashMap<String, Double>();
        LinkedHashMap<String, Double> lexiconDT = new LinkedHashMap<String, Double>();
        LinkedHashMap<String, Double> lexiconCOOC = new LinkedHashMap<String, Double>();
        try {
            BufferedReader readLexicon = new BufferedReader(new InputStreamReader(new FileInputStream(rootDirectory + "\\resources\\COOCPolarityHindi.txt"), "UTF-8"));
            String line = "";
            while ((line = readLexicon.readLine()) != null) {
                String tokens[] = line.split("\\|");
                if (lexicon.containsKey(tokens[0])) {
                    System.out.println("Error");
                } else {
                    lexicon.put(tokens[0], Double.parseDouble(tokens[1]));
                }
            }
            System.out.println(lexicon.size());
            readLexicon.close();

            BufferedReader read = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
            int count = 0;
        

            while ((line = read.readLine()) != null) {
                String tokens[] = line.split(" ");
                double pos = 0, neg = 0, neu = 0;
                double strongPos = 0, strongNeg = 0;
                double totalDT = 0, totalCOOC = 0;
                for (int i = 0; i < tokens.length; i++) {
                    double val;
                    double p1;
                    double p2;
                    if (lexicon.containsKey(tokens[i])) {
                        val = lexicon.get(tokens[i]);
                        
                        if (val > 0) {
                            pos++;
                            
                        } else if (val < 0) {
                            
                            neg++;
                            
                        } else if (val == 0) {
                            neu++;
                        }
                        }
                }
                featureVector.add(count, new LinkedHashMap<Integer, Double>());
                featureVector.get(count).put(1, pos);
                featureVector.get(count).put(2, neg);
                
                System.out.println(featureVector.get(count));
                featureCount = featureVector.get(count).size();
                count++;
            }
            read.close();
        } catch (IOException e) {
            System.out.println(e);
        }

        return featureVector;
    }

    public List<LinkedHashMap<Integer, Double>> getTrainingList() {
        return this.trainingFeature;
    }

    public List<LinkedHashMap<Integer, Double>> getTestList() {
        return this.testFeature;
    }

    public int getFeatureCount() {
        return featureCount;
    }

    private void setFeatureCount(int count) {
        this.featureCount = count;
    }

    public static void main(String[] args) throws IOException {
        COOCHindi ob = new COOCHindi("E:\\7th Sem\\NLP Project\\Sentiment");
    }
}