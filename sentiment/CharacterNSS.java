package sentimentClassifier;

import java.io.*;
import java.util.*;

public class CharacterNSS {
    List<LinkedHashMap<Integer, Double>> trainingFeature;
    List<LinkedHashMap<Integer, Double>> testFeature;
    String rootDirectory;

    int featureCount;

    CharacterNSS(String rootDirectory) throws IOException {
        this.rootDirectory = rootDirectory;
        trainingFeature = new ArrayList<LinkedHashMap<Integer, Double>>();
        testFeature = new ArrayList<LinkedHashMap<Integer, Double>>();

        LinkedHashMap<String, Integer> indexedNgrams = new LinkedHashMap<String, Integer>();
        indexedNgrams = generateCharacterNgrams();

        trainingFeature = generateFeature(indexedNgrams, rootDirectory + "\\dataset\\tokenized_Train.txt");

        getTrainingList();
        getTestList();

    }

    private LinkedHashMap<String, Integer> generateCharacterNgrams() throws IOException {
        LinkedHashMap<String, Integer> indexedNgram = new LinkedHashMap<String, Integer>();
        BufferedReader read = new BufferedReader(new InputStreamReader(new FileInputStream(rootDirectory + "\\dataset\\tokenized_Train.txt"), "UTF-8"));
        String line = "";
        String text = "";
        while ((line = read.readLine()) != null) {
            line.trim();
            text += line + " ";
        }

        LinkedHashMap<String, Integer> ngramMap = new LinkedHashMap<String, Integer>();

        int count = 1;

        if (text == null || text.isEmpty()) {
            throw new IllegalArgumentException("null or empty text");
        }
        String[] words = text.split(" ");
        List<String> list = new ArrayList<String>();

        for (int i = 0; i < words.length; i++) {
            words[i].trim();
            String ngram;
            if (words[i].length() >= 2) {
                ngram = words[i].substring(words[i].length() - 2);

                if (!indexedNgram.containsKey(ngram)) {
                    indexedNgram.put(ngram, count++);
                }
            }
        }


        indexedNgram = sortHashMapByValuesD(indexedNgram);

        System.out.println(indexedNgram);
        setFeatureCount(count);

        System.out.println("Func: " + featureCount);

        return indexedNgram;
    }


    private List<LinkedHashMap<Integer, Double>> generateFeature(LinkedHashMap<String, Integer> indexedNgram, String fileName) throws IOException {
        List<LinkedHashMap<Integer, Double>> featureVector = new ArrayList<LinkedHashMap<Integer, Double>>();
        try {
            BufferedReader read = new BufferedReader(new FileReader(new File(fileName)));
            String line = "";
            int count = 0;

            while ((line = read.readLine()) != null) {
                line = line.replace("\n", "").replace("\r", "");
                featureVector.add(count, new LinkedHashMap<Integer, Double>());
                String[] words = line.split(" ");

                for (int i = 0; i < words.length; i++) {
                    words[i].trim();
                    String ngram;
                    if (words[i].length() >= 2) {
                        ngram = words[i].substring(words[i].length() - 2);
                        if (indexedNgram.containsKey(ngram)) {
                            featureVector.get(count).put(indexedNgram.get(ngram).intValue(), 1.0);
                        }
                    }

                }
                count++;
            }


            for (int i = 0; i < featureVector.size(); i++)    //Print the feature values in sorted order
            {

                LinkedHashMap<Integer, Double> linkedHashMap = new LinkedHashMap<Integer, Double>();
                Map<Integer, Double> sortedMap = new TreeMap<Integer, Double>(featureVector.get(i));

                Set set = sortedMap.entrySet();
                Iterator iterator = set.iterator();
                featureVector.get(i).clear();
                while (iterator.hasNext()) {
                    Map.Entry mentry = (Map.Entry) iterator.next();
                    featureVector.get(i).put(Integer.parseInt(mentry.getKey().toString()), Double.parseDouble(mentry.getValue().toString()));
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        return featureVector;
    }


    public List<LinkedHashMap<Integer, Double>> getTrainingList() {
        return this.trainingFeature;
    }

    public List<LinkedHashMap<Integer, Double>> getTestList() {
        return this.testFeature;
    }

    private void setFeatureCount(int count) {
        this.featureCount = count;
    }

    public int getFeatureCount() {
        return featureCount;
    }


    private LinkedHashMap sortHashMapByValuesD(HashMap passedMap) {
        List mapKeys = new ArrayList(passedMap.keySet());
        List mapValues = new ArrayList(passedMap.values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);

        LinkedHashMap sortedMap =
                new LinkedHashMap();

        for (Object val : mapValues) {
            for (Object key : mapKeys) {
                String comp1 = passedMap.get(key).toString();
                String comp2 = val.toString();

                if (comp1.equals(comp2)) {
                    passedMap.remove(key);
                    mapKeys.remove(key);
                    sortedMap.put(key, val);
                    break;
                }

            }

        }
        return sortedMap;
    }

    public static void main(String[] args) throws IOException {
        CharacterNSS ob = new CharacterNSS(System.getProperty("user.dir"));
    }
}